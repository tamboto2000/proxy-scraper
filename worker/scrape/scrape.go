package scrape

import (
	"collin/spider/util"
	"proxy"
	"proxy_scraper/database/models"
	"runtime"
	"sync"

	"github.com/jinzhu/gorm"
)

type ScrapeProxy struct {
	db *gorm.DB
	wg *sync.WaitGroup
}

func New(db *gorm.DB) *ScrapeProxy {
	return &ScrapeProxy{db: db, wg: new(sync.WaitGroup)}
}

func (wk *ScrapeProxy) Run() {
	go func() {
		for {
			wk.run()
			wk.wg.Wait()
		}
	}()
}

func (wk *ScrapeProxy) run() {
	proxs, err := proxy.GetProxy("", proxy.AnonElite)
	if err != nil {
		wk.handleFatalError("internal_error", "fatal_error", err)
	}

	div := runtime.NumCPU()
	lim := len(proxs) / div
	counter := 0
	procc := make([]proxy.Proxy, 0)

	for _, prox := range proxs {
		procc = append(procc, prox)
		counter++

		if counter == lim {
			wk.wg.Add(1)
			go wk.check(procc)
			procc = make([]proxy.Proxy, 0)
			counter = 0
		}
	}

	if len(procc) > 0 {
		wk.wg.Add(1)
		go wk.check(procc)
	}
}

func (wk *ScrapeProxy) check(proxs []proxy.Proxy) {
	for _, prox := range proxs {
		if err := proxy.Test(prox); err != nil {
			continue
		} else {
			wk.handleFatalError("internal_error", "fatal_error", wk.db.Create(&models.Proxy{
				ID:        util.RandString(20),
				IP:        prox.IP,
				Port:      prox.Port,
				Code:      prox.Code,
				Country:   prox.Country,
				Anonimity: prox.Anonimity,
				HTTPS:     prox.Https,
				ProxyStr:  "http://" + prox.IP + ":" + prox.Port,
				InUse:     false,
			}).Error)
		}
	}

	wk.wg.Done()
}

func (wk *ScrapeProxy) handleFatalError(tag, ty string, err error) {
	if err != nil {
		wk.db.Create(&models.ScraperErrorLog{
			Scraper:      "proxy_scraper",
			ErrorID:      util.RandString(20),
			Type:         ty,
			Tag:          tag,
			Message:      err.Error(),
			Acknowledged: false,
		})
	}
}
