package controller

import (
	"encoding/json"
	"net/http"
	"proxy_scraper/database/models"
	"strings"

	"github.com/jinzhu/gorm"
)

type MarkAsError struct {
	DB *gorm.DB
}

func (ctrl *MarkAsError) AsError(w http.ResponseWriter, r *http.Request) {
	proxyID := r.URL.Query().Get("proxyID")
	if proxyID == "" {
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(map[string]interface{}{
			"status":     "bad request",
			"statusCode": 400,
			"message":    "parameter proxyID has invalid value",
		})

		return
	}

	proxyID = strings.Replace(proxyID, " ", "", -1)
	if !strings.Contains(proxyID, ",") {
		exist, err := checkProxyAvailable(ctrl.DB, proxyID)
		if err != nil {
			w.WriteHeader(500)
			json.NewEncoder(w).Encode(map[string]interface{}{
				"status":       "internal server error",
				"statusCode":   500,
				"errorMessage": err.Error(),
			})

			return
		}

		if !exist {
			w.WriteHeader(404)
			json.NewEncoder(w).Encode(map[string]interface{}{
				"status":     "not found",
				"statusCode": 404,
				"message":    "proxy with id " + proxyID + " is not found",
			})

			return
		}

		if err := deleteProxy(ctrl.DB, proxyID); err != nil {
			if err.Error() == gorm.ErrRecordNotFound.Error() {
				w.WriteHeader(404)
				json.NewEncoder(w).Encode(map[string]interface{}{
					"status":     "not found",
					"statusCode": 404,
					"message":    "proxy with id " + proxyID + " is not found",
				})

				return
			}

			w.WriteHeader(500)
			json.NewEncoder(w).Encode(map[string]interface{}{
				"status":       "internal server error",
				"statusCode":   500,
				"errorMessage": err.Error(),
			})

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"status": "OK",
			"statusCode": 200,
		})

		return
	}

	ids := splitID(proxyID)
	for _, id := range ids {
		exist, err := checkProxyAvailable(ctrl.DB, id)
		if err != nil {
			w.WriteHeader(500)
			json.NewEncoder(w).Encode(map[string]interface{}{
				"status":       "internal server error",
				"statusCode":   500,
				"errorMessage": err.Error(),
			})

			return
		}

		if !exist {
			w.WriteHeader(404)
			json.NewEncoder(w).Encode(map[string]interface{}{
				"status":     "not found",
				"statusCode": 404,
				"message":    "proxy with id " + id + " is not found",
			})

			return
		}

		if err := deleteProxy(ctrl.DB, id); err != nil {
			if err.Error() == gorm.ErrRecordNotFound.Error() {
				w.WriteHeader(404)
				json.NewEncoder(w).Encode(map[string]interface{}{
					"status":     "not found",
					"statusCode": 404,
					"message":    "proxy with id " + id + " is not found",
				})

				return
			}

			w.WriteHeader(500)
			json.NewEncoder(w).Encode(map[string]interface{}{
				"status":       "internal server error",
				"statusCode":   500,
				"errorMessage": err.Error(),
			})

			return
		}
	}

	json.NewEncoder(w).Encode(map[string]interface{}{
		"status": "OK",
		"statusCode": 200,
	})
}

func checkProxyAvailable(db *gorm.DB, id string) (bool, error) {
	if err := db.Take(new(models.Proxy), models.Proxy{ID: id}).Error; err != nil {
		if err.Error() == gorm.ErrRecordNotFound.Error() {
			return false, nil
		}

		return false, err
	}

	return true, nil
}

func splitID(dat string) []string {
	ids := make([]string, 0)
	stg1 := strings.Split(dat, ",")
	ids = stg1

	return ids
}

func deleteProxy(db *gorm.DB, id string) error {
	if err := db.Delete(new(models.Proxy), models.Proxy{ID: id}).Error; err != nil {
		return err
	}

	return nil
}
