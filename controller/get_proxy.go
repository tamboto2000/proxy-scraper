package controller

import (
	"collin/spider/util"
	"encoding/json"
	"net/http"
	"proxy_scraper/database/models"
	"strconv"

	"github.com/jinzhu/gorm"
)

type GetProxy struct {
	DB *gorm.DB
}

func (gt *GetProxy) Proxy(w http.ResponseWriter, r *http.Request) {
	limit := r.URL.Query().Get("limit")
	limitInt := 0
	if limit != "" {
		limit, err := strconv.Atoi(limit)
		if err != nil {
			w.WriteHeader(400)
			json.NewEncoder(w).Encode(map[string]interface{}{
				"status":     "bad request",
				"statusCode": 400,
				"message":    "pameter 'limit' has invalid value",
			})

			return
		}

		limitInt = limit
	}

	proxies, err := fetchProxy(gt.DB, limitInt)
	if err != nil {
		if err.Error() == gorm.ErrRecordNotFound.Error() {
			json.NewEncoder(w).Encode(map[string]interface{}{
				"status":     "OK",
				"statusCode": 200,
				"proxies":    proxies,
			})

			return
		}

		w.WriteHeader(500)
		json.NewEncoder(w).Encode(map[string]interface{}{
			"status":       "internal server error",
			"statusCode":   500,
			"errorMessage": err.Error(),
		})

		return
	}

	batchID := util.RandString(20)
	for _, prox := range proxies {
		if err := update(gt.DB, prox.ID); err != nil {
			w.WriteHeader(500)
			json.NewEncoder(w).Encode(map[string]interface{}{
				"status":       "internal server error",
				"statusCode":   500,
				"errorMessage": err.Error(),
			})

			return
		}
	}

	if err := createBatch(gt.DB, proxies, batchID); err != nil {
		w.WriteHeader(500)
		json.NewEncoder(w).Encode(map[string]interface{}{
			"status":       "internal server error",
			"statusCode":   500,
			"errorMessage": err.Error(),
		})

		return
	}

	json.NewEncoder(w).Encode(map[string]interface{}{
		"status":     "OK",
		"statusCode": 200,
		"proxies":    proxies,
		"batchID":    batchID,
	})
}

func update(db *gorm.DB, id string) error {
	return db.Model(new(models.Proxy)).Where("id = ?", id).Updates(models.Proxy{InUse: true}).Error
}

func createBatch(db *gorm.DB, proxs []models.Proxy, batchID string) error {
	for _, prox := range proxs {
		if err := db.Create(&models.ProxyInUse{
			BatchID: batchID,
			ProxyID: prox.ID,
		}).Error; err != nil {
			return err
		}
	}

	return nil
}

func fetchProxy(db *gorm.DB, lim int) ([]models.Proxy, error) {
	proxies := []models.Proxy{}
	if lim > 0 {
		err := db.Limit(lim).Find(&proxies, models.Proxy{InUse: false}).Error
		if err != nil {
			return proxies, err
		}
	}

	if lim <= 0 {
		err := db.Find(&proxies, models.Proxy{InUse: false}).Error
		if err != nil {
			return proxies, err
		}
	}

	return proxies, nil
}
