package controller

import (
	"encoding/json"
	"net/http"
	"proxy_scraper/database/models"

	"github.com/jinzhu/gorm"
)

type UnUse struct {
	DB *gorm.DB
}

func (ctrl *UnUse) Use(w http.ResponseWriter, r *http.Request) {
	batchID := r.URL.Query().Get("batchID")
	if batchID == "" {
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(map[string]interface{}{
			"status":     "bad request",
			"statusCode": 400,
			"message":    "pameter 'batchID' is empty",
		})

		return
	}

	batches, exist, err := checkBatch(ctrl.DB, batchID)
	if err != nil {
		w.WriteHeader(500)
		json.NewEncoder(w).Encode(map[string]interface{}{
			"status":       "internal server error",
			"statusCode":   500,
			"errorMessage": err.Error(),
		})

		return
	}

	if !exist {
		w.WriteHeader(404)
		json.NewEncoder(w).Encode(map[string]interface{}{
			"status":     "not found",
			"statusCode": 404,
			"message":    "batch not found",
		})

		return
	}

	if err := unuse(ctrl.DB, batches); err != nil {
		w.WriteHeader(500)
		json.NewEncoder(w).Encode(map[string]interface{}{
			"status":       "internal server error",
			"statusCode":   500,
			"errorMessage": err.Error(),
		})

		return
	}

	json.NewEncoder(w).Encode(map[string]interface{}{
		"status":     "OK",
		"statusCode": 200,
	})
}

func unuse(db *gorm.DB, batches []models.ProxyInUse) error {
	for _, batch := range batches {
		if err := db.Model(new(models.Proxy)).Where("id = ?", batch.ProxyID).Update("in_use", false).Error; err != nil {
			return err
		}
	}

	//delete batch data
	if err := db.Delete(&models.ProxyInUse{}, models.ProxyInUse{BatchID: batches[0].BatchID}).Error; err != nil {
		return err
	}

	return nil
}

func checkBatch(db *gorm.DB, id string) ([]models.ProxyInUse, bool, error) {
	batches := make([]models.ProxyInUse, 0)
	if err := db.Find(&batches, models.ProxyInUse{BatchID: id}).Error; err != nil {
		if err.Error() == gorm.ErrRecordNotFound.Error() {
			return batches, false, nil
		}

		return batches, false, err
	}

	return batches, true, nil
}
