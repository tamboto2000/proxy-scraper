package models

type Proxy struct {
	ID        string `gorm:"size:20" json:"id"`
	IP        string `json:"ip"`
	Port      string `json:"port"`
	Code      string `json:"code"`
	Country   string `json:"country"`
	Anonimity string `json:"anonimity"`
	HTTPS     bool   `json:"https"`
	ProxyStr  string `json:"proxyStr"`
	InUse     bool   `json:"inUse"`
}
