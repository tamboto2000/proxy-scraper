package models

import "time"

type ScraperErrorLog struct {
	ID           int       `gorm:"AUTO_INCREMENT" json:"-"`
	Scraper      string    `json:"scraper"`
	UserID       string    `gorm:"size:255" json:"userID"`
	RequestID    string    `gorm:"size:20" json:"requestID"`
	ErrorID      string    `gorm:"size:20" json:"errorID"`
	Type         string    `gorm:"type:enum('error', 'fatal_error')" json:"type"`
	Tag          string    `gorm:"size:255" json:"tag,omitempty"`
	UniqueID     string    `gorm:"size:255" json:"uniqueID,omitempty"`
	Message      string    `gorm:"type:mediumtext" json:"message"`
	Acknowledged bool      `gorm:"default:false" json:"acknowledged"`
	CreatedAt    time.Time `gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP" json:"-"`
	UpdatedAt    time.Time `gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" json:"-"`
}
