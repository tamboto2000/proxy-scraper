package models

type ProxyInUse struct {
	BatchID string `gorm:"size:20"`
	ProxyID string `gorm:"size:255"`
}
