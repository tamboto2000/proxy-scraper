package proxyDB

import (
	"fmt"
	"proxy_scraper/database/models"

	"github.com/jinzhu/gorm"
)

func DB() *gorm.DB {
	connArgs := fmt.Sprintf("%s:%s@(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", "danu", "t%5pg&RdNqw*CBRxe7Vi1ah", "31.220.61.116", "3306", "data_lake_admin")
	//connArgs := fmt.Sprintf("%s:%s@(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", "root", "kepler22b", "127.0.0.1", "3306", "test")

	db, err := gorm.Open("mysql", connArgs)
	if err != nil {
		panic(err.Error())
	}

	migration(db)

	return db
}

func migration(db *gorm.DB) {
	if err := db.AutoMigrate(
		models.Proxy{},
		models.ProxyInUse{},
	).Error; err != nil {
		panic(err.Error())
	}
}
