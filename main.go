package main

import (
	"net/http"
	"proxy_scraper/database/proxyDB"

	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var proxDB = proxyDB.DB()

func main() {
	//run workers
	RunWorkers()

	http.ListenAndServe(":8000", Routes())
}
