package main

import (
	"proxy_scraper/controller"

	"github.com/gorilla/mux"
)

func Routes() *mux.Router {
	root := mux.NewRouter()

	getProxy := controller.GetProxy{DB: proxDB}
	root.HandleFunc("/getProxy", getProxy.Proxy).Methods("GET")

	unUse := controller.UnUse{DB: proxDB}
	root.HandleFunc("/unUse", unUse.Use).Methods("GET")

	markAsError := controller.MarkAsError{DB:proxDB}
	root.HandleFunc("/markAsError", markAsError.AsError).Methods("GET")

	return root
}
