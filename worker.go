package main

import (
	"proxy_scraper/worker/scrape"
)

func RunWorkers() {
	scrape.New(proxDB).Run()
}
